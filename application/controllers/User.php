<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
//     function __construct(){
// 		parent::__construct();
	
//     }

public function __construct()
{
    parent::__construct();
            		// if($this->session->userdata('status') != "auth"){
            		// 	redirect(base_url("home"));
            		// }
        $this->load->model("product_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = 'Home';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data["products"] = $this->product_model->getAll();
        $this->load->view  ('templates/header', $data);
        // $this->load->view('templates/sidebar', $data);
        // $this->load->view('templates/topbar', $data);
        $this->load->view('user/home', $data);
        // $this->load->view('templates/footer', $data);
        // echo 'Selamat datang ' . $data['user']['name'];
    }

    function search() {
        $keyword    =   $this->input->post('cari'); //tergantung namenya apa
        $data['results']    =   $this->product_model->cari($keyword);
        $this->load->view('home',$data);
    }

   

    
}

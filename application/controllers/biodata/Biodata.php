<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Biodata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->form_validation->set_rules('nisn', 'nisn', 'required|trim');
        $this->form_validation->set_rules('nama', 'nama', 'required|trim');
        $this->form_validation->set_rules('ttl', 'ttl', 'required|trim');
        $this->form_validation->set_rules('perusahaan', 'perusahaan', 'required|trim');
        $this->form_validation->set_rules('alamat', 'alamat', 'required|trim');
        $this->form_validation->set_rules('kelas', 'kelas', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This email is already'
        ]);
        
        // $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
        //     'matches' => 'Password dont match!',
        //     'min_lenght' => 'password too short!'
        // ]);
        // $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Daftar PKL';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('user/data');
        } else {

            $data = [
                'nisn'=> htmlspecialchars($this->input->post('nisn', true)),
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'ttl' => htmlspecialchars($this->input->post('ttl', true)),
                'alamat' => htmlspecialchars($this->input->post('alamat', true)),
                'kelas' => htmlspecialchars($this->input->post('kelas', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'perusahaan' => htmlspecialchars($this->input->post('perusahaan', true)),
            ];
            $this->db->insert('biodata', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kamu Telah Terdaftar!
          </div>');
            redirect('user');
        }
    }

    }

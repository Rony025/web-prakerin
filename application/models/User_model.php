<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "biodata";

    public $id;
    public $nisn;
    public $nama;
    public $ttl;
    public $alamat;
    public $kelas;
    public $email;
    public $perusahaan;

    public function rules()
    {
        return [
            [
                'field' => 'nisn',
                'label' => 'nisn',
                'rules' => 'required'
            ],

            [
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required'
            ],

            [
                'field' => 'ttl',
                'label' => 'ttl',
                'rules' => 'required'
            ],

            [
                'field' => 'alamat',
                'label' => 'alamat',
                'rules' => 'required'
            ],
            [
                'field' => 'kelas',
                'label' => 'kelas',
                'rules' => 'required'
            ],
            [
                'field' => 'perusahaan',
                'label' => 'perusahaan',
                'rules' => 'required'
            ],
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required'
            ]
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id = uniqid();
        $this->nisn = $post["nisn"];
        $this->nama = $post["nama"];
        $this->ttl = $post["ttl"];
        $this->alamat = $post["alamat"];
        $this->kelas = $post["kelas"];
        $this->email = $post["email"];
        $this->perusahaan = $post["perusahaan"];
        $this->db->insert($this->_table, $this);
    }

    // public function update()
    // {
    //     $post = $this->input->post();
    //     $this->id = $post["id"];
    //     $this->name = $post["name"];
    //     $this->price = $post["price"];
    //     $this->description = $post["description"];
    //     $this->db->update($this->_table, $this, array('id' => $post['id']));
    // }

    // public function delete($id)
    // {
    //     return $this->db->delete($this->_table, array("id" => $id));
    // }
}

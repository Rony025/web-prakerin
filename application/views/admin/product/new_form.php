<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

	<title>login Products</title>

	<!-- Bootstrap core CSS-->
	<link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom fonts for this template-->
	<link href="<?= base_url() ?>assets/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<!-- Page level plugin CSS-->
	<link href="<?= base_url() ?>assets/datatables/dataTables.bootstrap4.css" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url() ?>css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top">

	<div id="wrapper">
		<div id="content-wrapper">

			<div class="container-fluid">

				<!-- Breadcrumbs-->
				<ol class="breadcrumb">


					<li class="breadcrumb-item ">
						<a href="<?= base_url() ?>index.php/admin">Admin</a>
					</li>


					<li class="breadcrumb-item ">
						<a href="<?= base_url() ?>index.php/admin/products">Products</a>
					</li>


					<li class="breadcrumb-item active">
						Add </li>
				</ol>


				<div class="card mb-3">
					<div class="card-header">
						<a href="<?= base_url() ?>index.php/admin/products/"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="name">Name*</label>
								<input class="form-control " type="text" name="name" placeholder="Product name" />
								<div class="invalid-feedback">
								</div>
							</div>

							<div class="form-group">
								<label for="price">Kuota*</label>
								<input class="form-control " type="number" name="price" min="0" placeholder="Product price" />
								<div class="invalid-feedback">
								</div>
							</div>


							<div class="form-group">
								<label for="name">Photo</label>
								<input class="form-control-file <?php echo form_error('price') ? 'is-invalid':'' ?>"
								 type="file" name="image" />
								 <!-- <input type="hidden" name="old_image" value="<?php echo $products->image ?>"/> -->
								<div class="invalid-feedback">
									<?php echo form_error('image') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Description*</label>
								<textarea class="form-control " name="description" placeholder="Product description..."></textarea>
								<div class="invalid-feedback">
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<!-- Sticky Footer -->
				<footer class="sticky-footer">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<span>SMK TELKOM PURWOKERTO</span>
						</div>
					</div>
				</footer>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fas fa-angle-up"></i>
		</a>

		<!-- Bootstrap core JavaScript-->
		<script src="<?= base_url() ?>assets/jquery/jquery.min.js"></script>
		<script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Core plugin JavaScript-->
		<script src="<?= base_url() ?>assets/jquery-easing/jquery.easing.min.js"></script>
		<!-- Page level plugin JavaScript-->
		<script src="<?= base_url() ?>assets/chart.js/Chart.min.js"></script>
		<script src="<?= base_url() ?>assets/datatables/jquery.dataTables.js"></script>
		<script src="<?= base_url() ?>assets/datatables/dataTables.bootstrap4.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="<?= base_url() ?>js/sb-admin.min.js"></script>
		<!-- Demo scripts for this page-->
		<script src="<?= base_url('assets/') ?>/datatables-demo.js"></script>
		<script src="<?= base_url('assets/') ?>/chart-area-demo.js"></script>

</body>

</html>